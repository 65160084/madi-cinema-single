import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
export default interface ticket{
    id: number
    name: string,
    where: string,
    date: string,
    time: string,
    movieType: number
    seat: string[]
}
let lastId = 1
export const useTicketStore = defineStore('ticket', () => {
    const ticketnow = ref<ticket>({id: lastId,name: '',where: '', date: '', time: '', movieType: 0, seat: ['']})
    const toshow = ref<ticket[]>([])
    const ticketName = (name: string) => {
        ticketnow.value.name = name
    }
    const ticketWhere = (name: string) => {
        ticketnow.value.where = name
    }
    const ticketDate = (name: string) => {
        ticketnow.value.date = name
    }
    const ticketTime = (name: string) => {
        ticketnow.value.time = name
    }
    const ticketMovieType = (name: number) => {
        ticketnow.value.movieType = name
    }
    const ticketSeat = (name: string[]) => {
        ticketnow.value.seat = name
    }
    const push2View = () => {
        lastId++
        toshow.value.push(ticketnow.value)
        clear()
    }
    const clear = () => {
        ticketnow.value = {id: lastId,name: '',where: '', date: '', time: '', movieType: 0, seat: ['']}
    }
    let onoff = ref(true)
    const on2off = () => {
    onoff.value = onoff.value = false
    }   
  return { ticketnow, ticketName, ticketWhere, ticketDate, ticketTime, ticketMovieType, ticketSeat, push2View, toshow, clear, onoff, on2off}
})
